# tkpod-modeller

[![conda](https://img.shields.io/conda/dn/tkpod/tkpod-modeller.svg)](https://anaconda.org/tkpod/tkpod-modeller/)
[![docs](https://img.shields.io/badge/docs-v0.1.5-blue.svg)](https://tkpod.gitlab.io/tkpod-modeller/v0.1.5/)
[![build status](https://gitlab.com/tkpod/tkpod-modeller/badges/v0.1.5/build.svg)](https://gitlab.com/tkpod/tkpod-modeller/commits/v0.1.5/)
[![coverage report](https://gitlab.com/tkpod/tkpod-modeller/badges/v0.1.5/coverage.svg)](https://tkpod.gitlab.io/tkpod-modeller/v0.1.5/htmlcov/)

Modeller plugin for `tkpod`.
