from setuptools import setup


def read_md(file):
    with open(file) as fin:
        return fin.read()


setup(
    name="tkpod-modeller",
    version="0.1.5",
    description="Modeller plugin for `tkpod`.",
    long_description=read_md("README.md"),
    author="Alexey Strokach",
    author_email="alex.strokach@utoronto.ca",
    url="https://gitlab.com/tkpod/tkpod-modeller",
    packages=["tkpod.plugins.modeller"],
    package_data={},
    include_package_data=True,
    zip_safe=False,
    keywords="tkpod",
    classifiers=[
        "Development Status :: 2 - Pre-Alpha",
        "Intended Audience :: Developers",
        "Natural Language :: English",
        "Programming Language :: Python :: 3.6",
    ],
    test_suite="tests",
)
